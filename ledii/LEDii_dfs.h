/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2019 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef __LEDII_DFS__
#define __LEDII_DFS__

#include "Arduino.h"

#define VERSION "LEDii 20190821 (c)Rocrail"
#define MAGIC   0x55

#define OPT_BLINK   0x10
#define OPT_DELAY   0x0F

#define FLAG_NONE   0
#define FLAG_UPDATE 1

typedef struct _MODCNF {
  byte magic;
  byte addr;
  byte flag;
} *pMODCNF;

typedef struct _LEDCNF {
  unsigned long rgb;
  byte opt;
  byte bri;
  byte bricur;
  byte brinew;
  byte flag;
} *pLEDCNF;

#endif
