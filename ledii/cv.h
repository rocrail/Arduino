/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2019 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _CV_H_
#define _CV_H_

#include "LEDii_dfs.h"

class CV
{
private:

public:
  CV();
  static void reset(unsigned int cnt);
  static unsigned char get(unsigned int  cv);
  static void set(unsigned int cv, unsigned char value);
  static void setLED(unsigned int addr, pLEDCNF cnf);
  static void getLED(unsigned int addr, pLEDCNF cnf);
  static void setMOD(pMODCNF cnf);
  static void getMOD(pMODCNF cnf);
};


#endif
