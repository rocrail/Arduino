/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2019 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#include "cv.h"
#include <EEPROM.h>

//---------- CV constructor; Not needed because all functions are static.
CV::CV() {
}


//---------- reset the first 1024 EEPROM bytes to zero.
void CV::reset(unsigned int cnt) {
  for( unsigned int i = 0; i < cnt; i++ ) {
    CV::set(i, 0);
  }
}


//---------- Get an EEPROM byte value.
unsigned char CV::get(unsigned int cv) {
  return EEPROM.read(cv);
}


//---------- Set an EEPROM byte value.
void CV::set(unsigned int cv, unsigned char value) {
  EEPROM.write(cv, value);
}


//---------- Get a two byte EEPROM value.
void CV::getLED(unsigned int led, pLEDCNF cnf) {
  unsigned int offset = led * sizeof(struct _LEDCNF) + sizeof(struct _MODCNF);
  EEPROM.get(offset, *cnf);
}


//---------- Set a two byte EEPROM value.
void CV::setLED(unsigned int led, pLEDCNF cnf) {
  unsigned int offset = led * sizeof(struct _LEDCNF) + sizeof(struct _MODCNF);
  EEPROM.put(offset, *cnf);
}


void CV::getMOD(pMODCNF cnf) {
  EEPROM.get(0, *cnf);
}
void CV::setMOD(pMODCNF cnf) {
  EEPROM.put(0, *cnf);
}
