# LEDii
Firmware to control LEDs on the [G-ASH03](https://wiki.rocrail.net/doku.php?id=2803-led-en) board. 

## Dependencies
The following libraries are rerquired:
- [FastLED](https://github.com/FastLED/FastLED)
- [RASCII](https://wiki.rocrail.net/doku.php?id=rascii-en)


## Rocrail
Use a [Rocrail](https://wiki.rocrail.net) revision of 15893 or higher.