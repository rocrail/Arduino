#ifndef RASCII_h
#define RASCII_h

#include "Arduino.h"

#define CMD '@'
#define EVT '$'
#define INF '#'
#define RSP '&'

#define CMD_INFO      0
#define CMD_START     1
#define CMD_STOP      2
#define CMD_GETCONF   3
#define CMD_SETCONF   4
#define CMD_RESETCONF 5
#define CMD_GETCV     6
#define CMD_SETCV     7
#define CMD_DOUT      10
#define CMD_AOUT      11
#define CMD_DIN       12
#define CMD_AIN       13
#define CMD_PWM       14
#define CMD_ACK       127

#define EVT_OCC   1
#define EVT_RFID  2
#define EVT_DIN   3
#define EVT_AIN   4
#define EVT_DSEN  5
#define EVT_ASEN  6

struct QueueItem {
  byte type;
  byte evt;
  byte len;
  byte data[32];
};

#define QUEUESIZE 10

class RASCII
{
  bool m_bSoftwareSerial;
  uint8_t  m_iSoftSerialRX;        //Serial Receive pin
  uint8_t  m_iSoftSerialTX;        //Serial Transmit pin
  uint8_t  m_iSoftSerialTxControl; //RS485 Direction control

  QueueItem Queue[QUEUESIZE];

  public:
    RASCII(bool softserial=false, uint8_t rxPin=0, uint8_t txPin=0, uint8_t txCtrl=0);
    void begin(long int baudrate);
    void transmit(bool txrx); // true=Tx, false=Rx
    bool read(byte* cmd);
    void write(const char* remark);
    void write(byte evt, byte* data, byte len, byte type);
    void queuePut(byte evt, byte* data, byte len, byte type);
    void queuePut(const char* remark);
    void queueWrite();
  private:
    byte HexA2Byte( const char* s );
    char* Byte2HexA(byte b, char* c);
    byte strLen(const char* s);
};

#endif


