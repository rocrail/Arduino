/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_cv_H_
#define _RCAN_cv_H_

#include "RCAN_dfs.h"

class CV
{
private:

public:
  CV();
  static void resetAll();
  static unsigned char get(unsigned int  cv);
  static unsigned int  getReg(unsigned int  cv);
  static void set(unsigned int cv, unsigned char value);
  static void setReg(unsigned int cv, unsigned int value);
};


#endif
