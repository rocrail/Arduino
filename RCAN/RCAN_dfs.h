/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCANDFS_H_
#define _RCANDFS_H_

#include <Arduino.h>

#define RCAN_VERMAJ 2
#define RCAN_VERMIN 1

#define null 0
#define uint unsigned int

#define DEFAULT_CANID 127

#define CAN_BUFFER_NOP  0
#define CAN_BUFFER_DATA 1

typedef struct CANFrame {
  uint32_t id;
  uint8_t  ext;
  uint8_t  dlc;
  uint8_t  data[8];
  uint8_t  state;
} * iCANFrame;

typedef struct CmdData {
  byte cmd; // internal code
  bool thisid;
  unsigned int id; // Node number, UID
  unsigned int addr;
  byte value;
  int ivalue;
  byte flags;
  byte datacnt;
  byte data[4];
} * iCmdData;


// General EEPROM variables
#define EE_SYNCID 0   // 1 byte SYNCID
#define EE_CONFIG 1   // 1 byte configuration
#define EE_CANID  2   // 1 byte CANID
#define EE_PORTCFG 8                    // 16 * 1 byte port configuration
#define EE_PORTSTAT1 EE_PORTCFG + 16    // 2 bytes port output status saved at power off command
#define EE_PORTSTAT2 EE_PORTSTAT1 + 1   
#define EE_BOARD      31  // 1 byte board config
#define EE_SERVO_POS  32  // 4 byte last servo position

// node config
#define CFG_SAVEOUTPUT 0x01
#define CFG_SHORTEVENTS 0x02
#define CFG_PULSETIME_MASK 0x0C
#define CFG_PULSETIME_250 0x00
#define CFG_PULSETIME_500 0x04
#define CFG_PULSETIME_1000 0x08
#define CFG_PULSETIME_2000 0x0C
#define CFG_SOD_REPORTALL 0x10
#define CFG_PULSETIME_LONG 0x20         // for 1500 3000 6000 12000ms Pulse Time

#define MTYP_CANGC2a   12 // 16 I/O
#define MTYP_CANGC6a   13 // 8 channel servo controller

// Boards
#define BOARD_MASK        0x07
#define BOARD_OPT_NONE    0x00
#define BOARD_OPT_SERVO   0x01

// Protocols
#define PROTOCOL_MASK   0x30
#define PROTOCOL_CBUS   0x00
#define PROTOCOL_MBUS   0x10
#define PROTOCOL_WIOC   0x20 // WIO CAN

// CLock speed
#define CLOCK_MASK  0x40
#define CLOCK_8MHZ  0x00
#define CLOCK_16MHZ 0x40

// USB Interface
#define USB_INTERFACE  0x80

#define MAX_FILTERS 2

#endif
