/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _WIOCAN_DFS_H_
#define _WIOCAN_DFS_H_

// Module types
#define MTYP_WIODCC     252
#define MTYP_WIOCAN     253
#define MTYP_CANSERVICE 254
#define MTYP_ROCRAIL    255


#define MANU_ROCRAIL  70   // https://www.rocrail.net


/* Instruction Code:
 
  IC = GRP | OPC

  GRP = (IC & 0xF0)
  OPC = (IC & 0x0F)
 */

#define GRP_SYS 0x00 // System
  #define SYS_NOTUSED 0x00 // Avoid a CANBUS bootloader conflict in case sid=8 and rid=4,5
                           // :X00080004N -> Bootloader command
                           // :X00080005N -> Bootloader data
  #define SYS_STATE   0x01
  #define SYS_SOD     0x02
  #define SYS_CLOCK   0x03
  #define SYS_EBREAK  0x04
  #define SYS_POWER   0x05

#define GRP_CNF 0x10 // Configuration
  #define CNF_QUERY  0x10
  #define CNF_GET    0x11
  #define CNF_SET    0x12
  #define CNF_NAME   0x13
  #define CNF_REBOOT 0x1F

#define GRP_PRG 0x20 // Programming
  #define PRG_GETCV 0x20
  #define PRG_SETCV 0x21
  #define PRG_PT    0x22

#define GRP_STR 0x30 // Streaming
  #define STR_TEXT_START 0x30 // datat[0] = display number
  #define STR_TEXT       0x31
  #define STR_FILE_START 0x32 // data = filename
  #define STR_FILE       0x33
  #define STR_PLAY       0x34
  #define STR_LIST       0x35

#define GRP_PRT 0x40 // Port I/O
  #define PRT_PORT   0x40
  #define PRT_SIGNAL 0x41

#define GRP_DCC 0x50 // DCC Command Station
  #define DCC_DIRV     0x50
  #define DCC_FUN      0x51
  #define DCC_BINSTATE 0x52
  #define DCC_ACC      0x53
  #define DCC_BIND     0x54

#define GRP_INF 0x60 // Unsolicited events like Sensors, RailCom, RFID scanner, ...
  #define INF_PORT 0x60
  #define INF_RFID 0x61
  #define INF_BIDI 0x62
  #define INF_ENV  0x63
  #define INF_IO   0x64 // Max. 64 I/O status which is mostly used for SoD.


// CNF index:
#define CNF_IDX_OPTIONS     0
#define CNF_IDX_PORTGROUP_0 1
#define CNF_IDX_PORTGROUP_1 2
#define CNF_IDX_PORTGROUP_2 3
#define CNF_IDX_PORTGROUP_3 4

// Frame arbitration
#define PRIO_SENSOR  0x00
#define PRIO_HIGH    0x01
#define PRIO_CONFIG  0x03
#define PRIO_LOW     0x05
#define PRIO_INFO    0x07

#define TYPE_CMD 0x00
#define TYPE_EVT 0x01
#define TYPE_RSP 0x02
#define TYPE_ACK 0x03

#define MASK_PRIO 0x1C000000 // >> 26
#define MASK_MODE 0x03000000 // >> 24
#define MASK_SID  0x00FF0000 // >> 16
#define MASK_IC   0x0000FF00 // >>  8
#define MASK_RID  0x000000FF // >>  0

#define SHIFT_PRIO 26
#define SHIFT_MODE 24
#define SHIFT_SID  16
#define SHIFT_IC    8
#define SHIFT_RID   0

// Extended 29bit ID: <PRIO 3bit> <TYPE 2bit> <SID 8bit>  <IC 8bit>   <RID 8bit>
//              Mask: <0x1C000000><0x03000000><0x00FF0000><0x0000FF00><0x000000FF>

// State bits
#define SYS_STATE_POWER 0x01
#define SYS_STATE_SC    0x02
#define SYS_STATE_PT    0x04
#define SYS_STATE_SLAVE 0x08


#endif
