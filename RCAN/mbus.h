/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_mbus_H_
#define _RCAN_mbus_H_

#include "RCAN_dfs.h"
#include "prot.h"

class MBus : public Prot
{
private:
  void makeFrame(iCANFrame frame, byte prio, unsigned int cmd, byte len, byte* data, bool rsp );
  unsigned int getCmd(iCANFrame frame, bool* rsp);
  void genHash();
protected:
  byte m_CANID;
  unsigned int  m_UID;
  unsigned char m_BoardType;
  unsigned long m_Hash;

public:
  MBus(byte ID, byte BoardType);
  bool evalFrame(iCANFrame frame, iCmdData cmd);
  bool makeFrame(iCANFrame frame, iCmdData cmd);
  int getCANBPS();
  void setID(byte id);
  byte getFilters(unsigned long* filters);
};


#endif
