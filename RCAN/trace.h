/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_trace_H_
#define _RCAN_trace_H_

// Disable to save a lot of memory!
#define DEBUG 1


// Global debug trace.
void trace(const char* fmt, ...);
void sendASCIIFrame(iCANFrame frame);
void dumpEEPROM(byte lines);
int getRevision();


#endif
