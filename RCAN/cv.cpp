/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#include "cv.h"
#include <EEPROM.h>

//---------- CV constructor; Not needed because all functions are static.
CV::CV() {
}


//---------- reset the first 1024 EEPROM bytes to zero.
void CV::resetAll() {
  for( unsigned char i = 0; i < 1024; i++ ) {
    CV::set(i, 0);
  }
}


//---------- Get an EEPROM byte value.
unsigned char CV::get(unsigned int cv) {
  return EEPROM.read(cv);
}


//---------- Set an EEPROM byte value.
void CV::set(unsigned int cv, unsigned char value) {
  EEPROM.write(cv, value);
}


//---------- Get a two byte EEPROM value.
unsigned int CV::getReg(unsigned int cv) {
  return ((EEPROM.read(cv) * 256) + EEPROM.read(cv+1));
}


//---------- Set a two byte EEPROM value.
void CV::setReg(unsigned int cv, unsigned int value) {
  EEPROM.write(cv+0, (value&0xFF00) >> 8 );
  EEPROM.write(cv+1, (value&0x00FF) );
}
