/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/

#define MANU_ROCRAIL  70   // https://www.rocrail.net
#define MTYP_CANGC2a  12 // 16 I/O
#define MTYP_CANGC6a  13 // 4 servo


#define GCA_BASE_UID 0x1C00 // GCA_BASE_UID + CANID = UID

#define PRIO_SYSTEM    0
#define PRIO_SENSORS   2
#define PRIO_ACCESSORY 4
#define PRIO_PING      6

#define MBUS_RESPONSE 0x01 // response flag 

#define CMD_SYSTEM            0x00
#define CMD_SYSSUB_STOP       0x00
#define CMD_SYSSUB_GO         0x01
#define CMD_SYSSUB_HALT       0x02
#define CMD_SYSSUB_EMBREAK    0x03
#define CMD_SYSSUB_ID         0x0C

#define CMD_READ_CONFIG       0x07
#define CMD_WRITE_CONFIG      0x08

#define CMD_ACC_SWITCH        0x0B
#define CMD_ACC_CONFIG        0x0C

#define CMD_PING              0x18
#define CMD_STATUS_DATA       0x1D

#define SENSOR_POLL   0x10
#define SENSOR_EVENT  0x11
#define ACC_CONFIG    0x0C

// Rocrail
#define ACC_QUERY           0x30
#define ACC_SUBCMD_PUBLISH  0x00
#define ACC_SUBCMD_GETCV    0x01
#define ACC_SUBCMD_SETCV    0x02
