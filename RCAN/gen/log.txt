688 2024-03-09 11:45:12 +0100 servo port correction
687 2024-03-08 12:05:16 +0100 pico binary
686 2024-03-08 09:44:42 +0100 CV restructured
685 2024-03-07 11:56:39 +0100 cv: offset fix for saving wifi
684 2024-03-07 11:36:15 +0100 Pico(W) wip
683 2024-03-06 15:42:48 +0100 gc2p type added
682 2024-03-06 15:20:38 +0100 LED 200ms pulse
681 2024-03-06 14:16:26 +0100 trace len extended
680 2024-03-06 11:47:27 +0100 pico (WIP)
679 2024-03-05 11:37:14 +0100 Pico compatible (WIP)
678 2024-03-05 09:43:46 +0100 io,RCAN: compile fix for Pico
677 2024-02-26 16:03:11 +0100 tracing update
676 2024-02-26 11:23:28 +0100 gpio: set cmd packet to zero before use
675 2022-01-23 10:42:55 +0100 RCAN: fix for setting 16MHz and USB
674 2022-01-01 15:26:46 +0100 copyright update
673 2021-12-28 11:55:03 +0100 RCAN: Servo support (WIP)
672 2021-12-27 15:38:16 +0100 servo option: WIP
671 2021-12-26 10:52:12 +0100 Servo support: WIP
670 2021-12-24 16:15:30 +0100 HEX for Nano Every
669 2021-12-24 14:30:48 +0100 RCAN: $ console command for setting port use
668 2021-12-24 13:44:19 +0100 RCAN: support for Nano Every
667 2021-12-22 14:07:29 +0100 GC2a schedule update
666 2021-12-21 10:27:15 +0100 RCAN binaries
665 2021-12-21 10:25:55 +0100 fix for SoD
664 2020-02-20 13:45:29 +0100 hex removed
663 2020-02-20 12:12:39 +0100 use loop for reading and writing to CAN instead of an ISR
662 2020-01-18 08:17:36 +0100 WIO: repo moved
661 2020-01-17 14:43:08 +0100 use restart for both architectures instead of reset for 8266
660 2020-01-17 11:58:58 +0100 OTA command, no longer per default active
659 2020-01-16 15:08:29 +0100 RCAN: disable debug
658 2020-01-16 14:37:20 +0100 fix for update MCP_CAN library
657 2020-01-15 15:35:56 +0100 booster option: wip
656 2020-01-13 12:14:26 +0100 disable all services in case of DCC type
655 2020-01-12 17:17:48 +0100 fix for calling IO init after a WIOSET command; cannot init twice
654 2020-01-12 08:43:20 +0100 swap board automatically to D32 in case of ESP32 and board D1
653 2020-01-11 19:10:11 +0100 Pins: fix for rotary pin2
652 2020-01-11 17:52:56 +0100 show WIO id on the status bar
651 2020-01-11 17:03:01 +0100 dynamic pin layout
650 2020-01-11 12:24:04 +0100 save selected loco index
649 2020-01-11 11:35:41 +0100 Pins: set throttle default layout to Lolin D32
648 2020-01-11 11:34:38 +0100 WIO 1.0
647 2020-01-10 16:49:33 +0100 WIO 1.0
646 2020-01-10 15:28:23 +0100 reorganise WIP
645 2020-01-09 16:35:44 +0100 show power symbol on statusbar for all types
644 2020-01-09 09:29:35 +0100 Sound: include typo fix
643 2020-01-08 18:08:25 +0100 save heap space
642 2020-01-08 17:43:24 +0100 use for html format PROGMEM to save heap
641 2020-01-08 16:05:28 +0100 free heap problem fix on esp8266
640 2020-01-08 14:07:38 +0100 Sound: disable I2C audio streaming -> too slow and drains the loop
639 2020-01-08 10:49:59 +0100 Display: fix for parameter length in case of B'id'
638 2020-01-08 09:20:47 +0100 version update
637 2020-01-08 09:17:38 +0100 Sound: stop current play in case of zero length sound file command
636 2020-01-08 09:03:56 +0100 sound: repeat implemented
635 2020-01-07 22:05:12 +0100 disable sound in case of throttle type
634 2020-01-07 19:25:13 +0100 fixe for FS totals: ESP32 and ESP8266 are not related...
633 2020-01-07 19:10:33 +0100 IO: select title for the status bar
632 2020-01-07 17:46:14 +0100 Sound: preparations for I2C MCP4725 DAC
631 2020-01-07 17:05:39 +0100 use names instead of numbers
630 2020-01-07 15:52:50 +0100 show total and used kb on html file listing
629 2020-01-07 11:15:03 +0100 I2C: simple mutex added to protect access
628 2020-01-07 09:36:59 +0100 I2C: noInterrupts can not be used to protect the I2C -> crash
627 2020-01-07 09:18:33 +0100 I2C corrections
626 2020-01-06 22:18:45 +0100 I2C protection
625 2020-01-06 18:29:11 +0100 centralise I2C I/O to be able to protect them by noIterrupt
624 2020-01-06 14:33:41 +0100 use I2C high speed of 1.7MHz
623 2020-01-06 12:07:21 +0100 sound: check if wav file is supported
622 2020-01-06 10:36:42 +0100 sound: play wav file
621 2020-01-05 16:53:14 +0100 sound: wip
620 2020-01-05 15:34:45 +0100 esp32: sound option (wip)
619 2020-01-04 11:09:31 +0100 displays: fix for parameter length
618 2020-01-04 10:39:07 +0100 disable touch option
617 2020-01-03 20:50:06 +0100 CV: resetFlash in case the magic number does not fit
616 2020-01-03 19:06:17 +0100 support for amp file names like {B'ns'}
615 2020-01-03 17:55:17 +0100 removed static user bitmaps
614 2020-01-03 15:29:46 +0100 esp32 spiffs corrections
613 2020-01-02 19:35:46 +0100 work around for 2.6.3 to make OTA visible
612 2020-01-02 16:58:00 +0100 html layout improvements
611 2020-01-02 14:12:28 +0100 use static unique variable names for host and ap
610 2020-01-02 13:12:46 +0100 html form corrections
609 2020-01-02 12:55:45 +0100 download without / path
608 2020-01-02 12:32:09 +0100 http download file added
607 2020-01-02 11:54:32 +0100 http upload and remove files added
606 2020-01-01 14:09:28 +0100 Rocrail logo as 999.amp
605 2020-01-01 13:53:10 +0100 fileutils: check if buffer size is sufficient for the bitmap
604 2020-01-01 11:57:04 +0100 fileutils: fix for reading last amp line in case of missing line feed
603 2020-01-01 09:54:11 +0100 example data
602 2020-01-01 09:36:12 +0100 fileutils: fix for parsing amp data
601 2019-12-31 23:35:20 +0100 display: {rn} command added to read and display an AMP file
600 2019-12-31 23:17:18 +0100 fileutils: read and parse AMP file
599 2019-12-31 22:52:58 +0100 copyright update
598 2019-12-31 16:00:17 +0100 file utilities
597 2019-12-30 12:28:06 +0100 throttle: fixed for multiple shift modes
596 2019-12-30 10:39:07 +0100 RASCII: set RUN flag in the example
595 2019-12-30 09:21:03 +0100 update icons and function shift (wip)
594 2019-12-29 19:18:30 +0100 throttle: show selected function + 1 in display
593 2019-12-29 19:09:54 +0100 throttle: removed auto light on at V > 0
592 2019-12-29 17:49:39 +0100 throttle: integrating function selection with rotary
591 2019-12-29 12:00:52 +0100 io: corrected version string
590 2019-12-29 11:49:28 +0100 draw rectangle to show the 100% update width
589 2019-12-29 10:53:21 +0100 update animating icon
588 2019-12-29 09:30:11 +0100 show OTA progress on the display
587 2019-12-28 18:29:06 +0100 throttle: fix for function flip with rotary switch
586 2019-12-28 11:57:28 +0100 not take over WIO i2c in case of throttle type
585 2019-12-28 09:09:05 +0100 option rotate added
584 2019-12-27 16:38:52 +0100 wakeup pin board dependent
583 2019-12-27 16:00:05 +0100 Lolin D32 support
582 2019-12-27 12:59:20 +0100 throttle: adjust rotary position after shift to match the velocity pos
581 2019-12-27 12:14:09 +0100 throttle: fix for shift and f0
580 2019-12-27 12:07:51 +0100 throttle: select with shift+rotary a function and flip this with shift+f0
579 2019-12-26 16:39:36 +0100 WIOCtrl schedule and pcb
578 2019-12-26 13:54:25 +0100 moved I/O to the IO class, normalised file header
577 2019-12-25 17:26:50 +0100 bitmap size normalisation
576 2019-12-25 11:29:10 +0100 throttle: switch between loco1 and loco2 with shift+rotarysw
575 2019-12-25 10:51:05 +0100 reorg and some improvements
574 2019-12-24 19:13:51 +0100 display: adjust column after drawing a bitmap
573 2019-12-24 17:21:21 +0100 battery bottom level correction
572 2019-12-24 14:57:45 +0100 only instantiate options if needed
571 2019-12-24 13:28:57 +0100 reduce memory usage for ESP8266
570 2019-12-24 09:19:30 +0100 display: battery fill correction
569 2019-12-23 19:20:05 +0100 shutdown event support
568 2019-12-23 18:06:00 +0100 state event
567 2019-12-23 15:55:52 +0100 keep contrast display option fix for battery percentage
566 2019-12-23 11:50:39 +0100 sync option added
565 2019-12-23 10:41:23 +0100 throttle: ignore events with same throttleid
564 2019-12-23 10:05:59 +0100 disable loco events
563 2019-12-23 08:49:50 +0100 fix for rotary switch and none extended layout
562 2019-12-22 18:33:47 +0100 power commands support
561 2019-12-22 17:31:03 +0100 font fix for number '1'
560 2019-12-22 17:12:08 +0100 process loco events (wip)
559 2019-12-22 14:07:08 +0100 set display in sleep mode in case of sleep timer
558 2019-12-22 12:28:28 +0100 deepsleep option added
557 2019-12-22 11:53:32 +0100 show empty battery in case < 5% and go into deep sleep
556 2019-12-22 11:28:42 +0100 check for low battery and force deep sleep
555 2019-12-22 11:22:56 +0100 default sleep 5 minutes
554 2019-12-22 11:19:46 +0100 goodbye event before deep sleep
553 2019-12-22 10:59:50 +0100 show version
552 2019-12-22 10:23:57 +0100 readme update
551 2019-12-22 09:40:24 +0100 show sleep start with a pause icon
550 2019-12-22 09:15:03 +0100 clock offset
549 2019-12-22 09:05:20 +0100 show begin of deep sleep in the display
548 2019-12-21 19:28:56 +0100 numbers made fixed width
547 2019-12-21 17:49:48 +0100 5 minutes default sleep timer
546 2019-12-21 17:47:57 +0100 show model time in the throttle display
545 2019-12-21 17:19:17 +0100 deepsleep added in case of throttle mode
544 2019-12-21 15:16:37 +0100 trace: dump flash fix
543 2019-12-21 15:06:33 +0100 show statusbar on all WIO types as long as no display command did came in
542 2019-12-21 13:29:38 +0100 throttle: selection methode changed
541 2019-12-21 08:51:35 +0100 wio: update statusbar fix for DCC type
540 2019-12-21 08:32:13 +0100 pins: simplified
539 2019-12-20 17:51:51 +0100 trace: fix for flash dump
538 2019-12-20 16:39:19 +0100 power status bitmaps improved
537 2019-12-20 14:57:21 +0100 use all pixels of the battery/load icon
536 2019-12-20 13:55:29 +0100 DCC display
535 2019-12-20 09:48:07 +0100 throttle: touch support added
534 2019-12-20 08:34:07 +0100 smaller function icons, and use only touch pins for function buttons
533 2019-12-20 07:52:53 +0100 function bitmaps resized from 8x8 to 7x7
532 2019-12-19 17:35:29 +0100 throttle: display layout change
531 2019-12-19 15:27:22 +0100 throttle: use shift for f9...f16
530 2019-12-19 14:10:53 +0100 throttle: F0 fixes and function offset
529 2019-12-19 11:21:10 +0100 throttle: update statusbar asap
528 2019-12-19 11:11:16 +0100 throttle: display layout
527 2019-12-19 09:50:30 +0100 throttle status bar and big numbers
526 2019-12-18 22:04:11 +0100 throttle: check for valid locoid range before trying to display it
525 2019-12-18 21:59:44 +0100 throttle: ignore shift+rotary in case no shortids are available
524 2019-12-18 17:52:21 +0100 converted png to bmp
523 2019-12-18 16:56:33 +0100 bitmaps for display
522 2019-12-18 14:51:12 +0100 throttle: select loco
521 2019-12-18 13:23:05 +0100 shortIDs support WIP
520 2019-12-17 15:33:29 +0100 update RSSI before query response and alive event
519 2019-12-17 15:03:29 +0100 put servo control in its own class
518 2019-12-17 13:28:24 +0100 ESP32 throttle support WIP
517 2019-12-17 11:50:05 +0100 WIP
516 2019-12-16 18:31:56 +0100 extended layout WIP
515 2019-12-16 14:34:48 +0100 RIC: extended layout WIP
514 2019-12-15 11:34:17 +0100 ESP32 Dev Module support (WIP)
513 2019-12-15 10:30:46 +0100 ric esp32 test
512 2019-12-14 13:12:03 +0100 use short address 111 for triggering decoders to broadcast their addresses
511 2019-12-14 10:41:10 +0100 option corrections
510 2019-12-13 15:58:08 +0100 DCC display update
509 2019-12-13 12:23:35 +0100 optional percent getBattery parameter
508 2019-12-12 16:34:11 +0100 set display geometry by dialog
507 2019-12-12 10:15:56 +0100 WIOBoost removed; replacement is WIODrive
506 2019-12-11 15:16:31 +0100 DCC: short address support for binstate
505 2019-12-11 12:49:32 +0100 DCC: send every 5 seconds a XF2 off to discover loco addresses
504 2019-12-11 11:11:09 +0100 railcom small corrections
503 2019-12-10 15:10:31 +0100 DCC corrections
502 2019-12-10 15:08:20 +0100 railcom read corrections
501 2019-12-10 14:58:13 +0100 hardware update
500 2019-12-09 16:40:55 +0100 hardware update WIODrive
499 2019-12-09 16:26:24 +0100 hardware update WIODrive
498 2019-12-09 12:01:54 +0100 added nodename to a mobile rfid event
497 2019-12-09 11:26:02 +0100 get/set nodename
496 2019-12-08 21:39:26 +0100 hardware update: WIODrive
495 2019-12-08 12:12:43 +0100 analog threshold added for measuring the booster current
494 2019-12-07 19:03:51 +0100 hardware update with WIODrive
493 2019-12-07 18:05:09 +0100 add WIOBoost current on query
492 2019-12-07 17:53:24 +0100 booster 1A option added
491 2019-12-07 16:48:34 +0100 put the ID12 reader in its own class to make it independent from Mobile
490 2019-12-07 15:01:38 +0100 last mobile pin change...
489 2019-12-07 13:57:45 +0100 fix for battery percentage in case of none RIC mode
488 2019-12-07 12:18:58 +0100 mobile: removed option mode1
487 2019-12-07 11:37:46 +0100 added mobile mode 1 option
486 2019-12-07 11:13:59 +0100 mobile: use L6203 mode
485 2019-12-07 08:32:59 +0100 reorg options
484 2019-12-06 19:40:25 +0100 options reorg WIP
483 2019-12-06 15:47:45 +0100 mobile battery percentage added
482 2019-12-06 14:44:57 +0100 mobile step range added to simulate cv2/cv5
481 2019-12-06 11:16:22 +0100 mobile rfid reading added
480 2019-12-05 18:54:42 +0100 USERCNF.h update
479 2019-12-05 18:02:26 +0100 mobile direction piun changed to D7
478 2019-12-05 15:19:40 +0100 DCC fixes and boost WIP
477 2019-12-05 14:25:27 +0100 fix for the mobile direction phase
476 2019-12-05 14:22:12 +0100 fix for alive ack ticker
475 2019-12-05 08:03:47 +0100 stop mobile in case of server lost or ack timeout
474 2019-12-04 22:02:57 +0100 removed inverted mobile PWM
473 2019-12-04 12:40:37 +0100 use AP password for OTA
472 2019-12-04 11:51:06 +0100 OTA added
471 2019-12-04 10:59:42 +0100 reordered mobile pins
470 2019-12-04 10:33:19 +0100 disabled the V magic invert
469 2019-12-04 09:12:03 +0100 start AP first after wifi connect attempt
468 2019-12-04 08:08:16 +0100 automatic shutdown of AP service after submit or 2 min. timeout
467 2019-12-03 19:27:56 +0100 shutdown the AP after submit or 2 minutes
466 2019-12-03 18:22:16 +0100 added Dagnalls direction magic
465 2019-12-03 15:51:57 +0100 mobile trace improvement
464 2019-12-03 14:39:21 +0100 fix for setting name by http
463 2019-12-03 14:29:46 +0100 ap fix for http form
462 2019-12-03 13:21:25 +0100 esp32 compatible web server
461 2019-12-03 12:33:27 +0100 access point function added for setting up the basic connection with http
460 2019-12-02 15:37:22 +0100 mobile: steerwheel angle added to the dirv command
459 2019-12-02 15:07:09 +0100 dirv: backwards compatibility added for steps <= 127
458 2019-12-02 15:03:42 +0100 mobile: allow 250 speed steps
457 2019-12-02 13:13:54 +0100 mobile: servo motor control
456 2019-12-02 12:29:18 +0100 mobile: servo motor speed support (wip)
455 2019-12-02 12:14:46 +0100 mobile: servo motor control option added
454 2019-12-01 13:13:57 +0100 mobile: mass parameter for accelleration
453 2019-12-01 09:58:48 +0100 mobile functions: f0 on board, f1...f32 I2C
452 2019-11-30 14:41:04 +0100 monitor help updated for mobile type
451 2019-11-30 14:10:51 +0100 mobile: use 20kHz and keep at max speed the block wave
450 2019-11-30 11:37:01 +0100 mobile type added to the query
449 2019-11-30 11:10:59 +0100 mobile type added to provide a simple PWM motor management
448 2019-11-29 14:58:01 +0100 added product IDs for WIO, RIC and Boost
447 2019-11-27 18:15:39 +0100 query with environment
446 2019-11-26 16:03:44 +0100 sensor mode fix
445 2019-11-26 15:09:28 +0100 add humidity to the alive event
444 2019-11-26 14:43:03 +0100 sensor: fix for reading humidity
443 2019-11-26 10:30:51 +0100 try to calculate humidity...
442 2019-11-25 11:21:05 +0100 RASCII array fix
441 2019-11-25 10:43:48 +0100 sensor pressure added to the alive event
440 2019-11-24 14:37:21 +0100 fix for RASCII default parameters (RIC)
439 2019-11-24 14:18:17 +0100 get pressure added
438 2019-11-24 10:57:59 +0100 bmp280 support added for temperature reporting
437 2019-11-23 10:43:25 +0100 avoid interpreting XML
436 2019-11-23 10:22:37 +0100 railcom: fix for bidi address report
435 2019-11-23 08:34:13 +0100 railcom depart fix
434 2019-11-23 08:03:20 +0100 railcom reader hardware gca214
433 2019-11-22 10:38:10 +0100 set railcom reader with the WIO dialog
432 2019-11-22 08:41:34 +0100 railcom: WIP
431 2019-11-21 11:35:20 +0100 railcom: report POM
430 2019-11-21 11:21:12 +0100 railcom update
429 2019-11-20 15:32:59 +0100 railcom: report stable address from channel 1
428 2019-11-20 12:34:55 +0100 railcom: WIP
427 2019-11-20 11:15:12 +0100 railcom: WIP
426 2019-11-20 07:54:04 +0100 AIN0 defined
425 2019-11-19 11:16:15 +0100 use input_pullup for throttle
424 2019-11-19 10:10:37 +0100 check if the BUILDIN_LED is defined for ESP32
423 2019-11-19 07:55:45 +0100 hardware update
422 2019-11-18 19:45:49 +0100 hardware update
421 2019-11-18 14:24:43 +0100 simplified the display I2C write
420 2019-11-18 12:02:56 +0100 removed invalid comment chars
419 2019-11-18 10:53:33 +0100 platform comments added
418 2019-11-18 10:40:27 +0100 pin corrections for esp32
417 2019-11-18 10:22:26 +0100 display prototype fix, trace write serial fix
416 2019-11-18 09:07:16 +0100 lolin D32 compatible
415 2019-11-17 15:22:18 +0100 set nodelay for sending small tcp/ip packets
414 2019-11-16 08:09:00 +0100 servo attach call only if needed by checking the attached function
413 2019-11-15 15:24:17 +0100 switch off the servo output after detach
412 2019-11-15 13:17:06 +0100 servo detach after 1000ms
411 2019-11-15 12:10:13 +0100 fix for servo detach to avoid a spastic move
410 2019-11-14 14:34:25 +0100 bidi event added
409 2019-11-14 13:53:04 +0100 trace level for doLED set to INFO
408 2019-11-14 08:02:51 +0100 individual blink delay
407 2019-11-13 15:17:57 +0100 fix for stop blink digital output
406 2019-11-13 14:47:36 +0100 digital output blink support added
405 2019-11-12 10:24:43 +0100 maxpower option added, fix for alive ping
404 2019-11-11 10:13:52 +0100 convert rssi into int16_t
403 2019-11-10 11:09:36 +0100 programming POM with 16bit cv
402 2019-11-07 11:07:57 +0100 servo ticker at 10ms, and rotary scan at 1ms
401 2019-11-06 14:50:59 +0100 only trace if room is left on the serial device
400 2019-11-06 12:04:38 +0100 default debug mode off
399 2019-11-06 10:57:21 +0100 fix for servo detach
398 2019-11-06 08:47:49 +0100 long click usercnf setting added, defaul 20 x 100ms
397 2019-11-05 14:43:26 +0100 LED global vars on top of source
396 2019-11-05 12:25:27 +0100 railcom option and code table added
395 2019-11-05 08:56:59 +0100 r2rnet removed: not possible to use
394 2019-11-05 08:46:37 +0100 trace option for use wifi only
393 2019-11-05 07:49:40 +0100 wio: pcb update
392 2019-11-04 15:00:29 +0000 Add LICENSE
391 2019-11-04 15:45:48 +0100 throttle: shorter time for accepting the function buttons
390 2019-11-04 15:16:59 +0100 monitor: new default speed 57600
389 2019-11-03 14:41:12 +0100 show battery(A0) in the settings ?<enter>
388 2019-11-03 14:26:40 +0100 native io option removed battery range in usercnf.h
387 2019-11-03 07:42:11 +0100 wio: hardware update
386 2019-11-02 16:07:13 +0100 fix for scan the I2C
385 2019-11-02 15:33:04 +0100 fix for setting io type
384 2019-11-02 12:33:30 +0100 binstate dcc command added
383 2019-11-02 11:07:27 +0100 readme update
382 2019-11-02 11:02:54 +0100 readme update
381 2019-11-01 15:07:54 +0100 reset alive response timer after getting a command
380 2019-11-01 14:26:17 +0100 dcc: only send lights/f0 on VDir command in case not already in the queue
379 2019-10-31 15:50:18 +0100 display: use RocDisplay fonts except for the 5x5
378 2019-10-31 14:52:20 +0100 hardware: no revision information in the filenames
377 2019-10-31 14:49:36 +0100 hardware readme update
376 2019-10-31 14:41:48 +0100 hardware: renamed RICi to RIC
375 2019-10-31 14:39:46 +0100 hardware readme update
374 2019-10-31 14:36:10 +0100 WIO hardware readme added
373 2019-10-31 14:07:05 +0100 WIO partlist
372 2019-10-31 12:19:34 +0100 led: signal support dimming improved
371 2019-10-31 11:01:45 +0100 dcc: accessory command queue added
370 2019-10-31 10:40:09 +0100 fastclock corrections
369 2019-10-31 10:22:07 +0100 7x5 font added
368 2019-10-30 16:22:46 +0100 dcc: purge inactive locos from the refresh stack
367 2019-10-30 15:20:18 +0100 license text added
366 2019-10-30 15:04:49 +0100 fix for signal aspact value evaluation
365 2019-10-30 10:47:58 +0100 dcc: show accessory commands on the display
364 2019-10-30 10:15:25 +0100 dcc: accessory command added
363 2019-10-29 12:21:26 +0100 io: attach servo only on command
362 2019-10-29 10:15:41 +0100 led: signal support up to 6 sub leds
361 2019-10-29 08:35:43 +0100 led: signal support (wip)
360 2019-10-28 10:24:22 +0100 wio: led signal support (WIP)
359 2019-10-27 11:33:10 +0100 dcc: alive events must be acknowledged to keep power on
358 2019-10-27 10:33:43 +0100 servo: range increased to 700...2300
357 2019-10-27 10:04:33 +0100 docu and hardware update
356 2019-10-26 08:28:13 +0200 docu update, default i2cled instead of throttle
355 2019-10-25 20:49:50 +0200 docu update
354 2019-10-25 18:17:07 +0200 docu update
353 2019-10-25 15:34:39 +0200 docu update
352 2019-10-25 10:03:58 +0200 cleanup
351 2019-10-25 07:43:45 +0200 dcc: fix for cutout
350 2019-10-25 04:40:34 +0200 project rename
349 2019-10-24 15:39:26 +0200 docu update
348 2019-10-24 15:20:18 +0200 cv set for dcc short circuit detection
347 2019-10-24 14:11:31 +0200 docu update
346 2019-10-24 11:27:39 +0200 dcc: sc check flag added
345 2019-10-24 10:44:44 +0200 RFID: skip scan if reader is not connected
344 2019-10-24 08:36:24 +0200 reset flash moved to the CV class
343 2019-10-24 08:28:12 +0200 railcom: WIP
342 2019-10-23 15:44:30 +0200 io: detach servo in case the wanted position has been reached
341 2019-10-23 15:27:35 +0200 reset/restart CV added
340 2019-10-23 12:29:43 +0200 docu update
339 2019-10-23 12:19:54 +0200 docu update
338 2019-10-23 11:56:58 +0200 docu update
337 2019-10-23 11:25:57 +0200 docu update
336 2019-10-23 10:52:23 +0200 wio docu added
335 2019-10-23 08:50:25 +0200 servo: check out of range position at startup
334 2019-10-23 08:25:13 +0200 dcc: short circuit recovery
333 2019-10-22 15:30:14 +0200 dcc: CDE compatible booster connection
332 2019-10-22 14:36:03 +0200 dcc: extra output for booster power control
331 2019-10-22 14:08:12 +0200 display: 7-segment clock added
330 2019-10-22 11:29:44 +0200 display: 7 segment fast clock {f} (WIP)
329 2019-10-21 15:46:08 +0200 dcc: POM set/get added
328 2019-10-21 15:10:13 +0200 dcc: pom write/read (wip)
327 2019-10-21 12:25:23 +0200 display: command {d} added to scroll one text line down
326 2019-10-21 11:14:01 +0200 dcc: moved pins to D6 and D7 to be conform with the WIO pcb
325 2019-10-21 11:06:21 +0200 dcc: power off in case of Rocrail Server connection loss
324 2019-10-21 10:28:12 +0200 dcc: support for 28 speed steps added
323 2019-10-21 08:29:51 +0200 dcc: display support added
322 2019-10-20 14:01:03 +0200 dcc: drive commands has higher prio then functions
321 2019-10-20 11:19:16 +0200 dcc: bus parameter for loco commands added
320 2019-10-20 10:28:43 +0200 dcc: mobile function support
319 2019-10-19 15:05:21 +0200 dcc: function command for long address added hardware: WIO update for DCC
318 2019-10-19 12:13:08 +0200 dcc: first loco move :)
317 2019-10-19 06:02:37 +0200 readme update
316 2019-10-18 14:40:22 +0200 dcc: dirv added
315 2019-10-18 13:33:32 +0200 dcc: wip
314 2019-10-18 10:02:14 +0200 dcc: wip
313 2019-10-17 14:23:06 +0200 display: better circle function
312 2019-10-17 12:12:45 +0200 hardware: revert WIO eagle to layo
311 2019-10-17 12:02:09 +0200 hardware update WIO
310 2019-10-17 11:21:05 +0200 display: ignore white space chars < blank
309 2019-10-17 08:56:28 +0200 display: multi command support
308 2019-10-16 16:40:05 +0200 io: fix for fixed servo test index (thanks to Dagnall)
307 2019-10-16 14:51:19 +0200 hardware update
306 2019-10-16 14:24:59 +0200 WIO PCB update
305 2019-10-16 10:46:21 +0200 display: scrolling text
304 2019-10-16 07:58:09 +0200 initial display geometry option added
303 2019-10-15 21:26:57 +0200 display: fix for the clear function in case of dual displays
302 2019-10-15 14:24:29 +0200 display: type correction
301 2019-10-15 10:06:44 +0200 display: code page byte added
300 2019-10-15 08:55:09 +0200 display: support for iso latin
299 2019-10-15 08:22:01 +0200 display: support for two SSD1306 on 0x3C and 0x3D
298 2019-10-14 16:02:44 +0200 display: support for smaller font {F1}
297 2019-10-14 14:58:12 +0200 display: font width fixes
296 2019-10-14 14:40:20 +0200 display: fix for clock and type 128x64 added
295 2019-10-14 11:14:20 +0200 display: geomtry command added
294 2019-10-13 15:47:06 +0200 display: inverse command added
293 2019-10-13 15:38:26 +0200 display: H commando for contrast added
292 2019-10-13 15:27:07 +0200 display: brightness used from clock event
291 2019-10-13 14:43:07 +0200 display: use lower case i for toggle invert character
290 2019-10-13 14:34:04 +0200 display: rotation 18° support and clock with lower case c
289 2019-10-13 12:54:10 +0200 clock sync added
288 2019-10-13 11:21:24 +0200 text type RASCII added to enable long formatted text
287 2019-10-13 10:20:29 +0200 clock: WIP
286 2019-10-12 15:33:53 +0200 text and graphics (wip)
285 2019-10-12 12:22:12 +0200 text support (WIP)
284 2019-10-11 15:13:41 +0200 welcome message
283 2019-10-11 14:57:38 +0200 display: printf added
282 2019-10-11 14:43:59 +0200 display: fonts and string function added
281 2019-10-11 10:01:00 +0200 display option added (wip)
280 2019-10-07 17:08:12 +0200 hardware update
279 2019-10-07 15:13:04 +0200 ignore same rfid tags within a pending off delay
278 2019-10-07 14:08:42 +0200 rfid: no using reset pin, slave select d3+d0
277 2019-10-07 12:22:51 +0200 slave select movedd from D8 to D4 to be able LEDs in combination with RFID
276 2019-10-07 10:21:30 +0200 show SN from RFID readers at the ? command
275 2019-10-07 08:33:37 +0200 reset pending acknowledged RFID after 1500ms
274 2019-10-06 14:35:20 +0200 extra delay after a rfid report to give wifi time to process
273 2019-10-06 14:25:12 +0200 report rfid off after ack with extra flag
272 2019-10-06 08:21:21 +0200 LOLii: set ID in the EBreak packet
271 2019-10-05 11:42:56 +0200 LOLii: report RFID off after ack
270 2019-10-05 11:21:28 +0200 LOLii: support for i2crfid sub type
269 2019-10-05 11:01:00 +0200 readme update
268 2019-10-05 10:55:58 +0200 readme update
267 2019-10-05 10:54:47 +0200 update readme
266 2019-10-05 10:43:12 +0200 LOLii: verify if RFID events are acknowledged and resent 5 times
265 2019-10-05 09:00:13 +0200 LOLii: fix for throttle crash
264 2019-10-05 08:09:49 +0200 LOLii: 2 RFID readers on SPI
263 2019-10-04 14:12:36 +0200 rfid: event added
262 2019-10-04 12:36:09 +0200 RFID added
261 2019-10-04 10:03:41 +0200 servo type added
260 2019-10-03 14:39:08 +0200 WIO: hardware update
259 2019-10-03 14:20:35 +0200 WIO: init saved servo position before attach
258 2019-10-03 10:30:32 +0200 LOLii: servo delay range check 1...10
257 2019-10-03 08:55:40 +0200 WIO: servo support
256 2019-10-02 16:04:16 +0200 WIO: report servo position reached
255 2019-10-02 15:01:39 +0200 WIO: servo support -> delay added
254 2019-10-02 14:07:58 +0200 WIO: basic servo support
253 2019-10-02 09:55:16 +0200 LOLii: add active loco address in the alive message
252 2019-10-02 08:17:13 +0200 LOLii: stop at 3.3V to save lipo lifetime
251 2019-10-01 15:39:07 +0200 LOLii: dump command added
250 2019-10-01 10:44:13 +0200 LOLii: replaced Serial.printf with level tracing
249 2019-09-30 15:47:29 +0200 LOLii: set WiFi power to zero
248 2019-09-30 14:53:49 +0200 WIO: hardware update
247 2019-09-30 11:36:53 +0200 LOLii: add battery and rssi on query
246 2019-09-30 10:36:15 +0200 LOLii: send V0 to both loco 1 and 2 in case of battery 3.2V
245 2019-09-30 10:05:09 +0200 LOLii: stop at 3.2V A0=660
244 2019-09-28 17:50:10 +0200 goodbye event added
243 2019-09-28 10:24:14 +0200 LOLii: switching between loco 1 and 2
242 2019-09-27 14:36:44 +0200 LOLii: prototype pin layout and accu level at alive
241 2019-09-27 11:27:19 +0200 LOLii: 4 second alive ping
240 2019-09-27 07:53:55 +0200 LOLii: revert LED offset 100
239 2019-09-27 07:39:09 +0200 LOLii: user steps added
238 2019-09-26 11:55:10 +0200 LOLii: native setup support added
237 2019-09-26 11:04:17 +0200 LOLii: native mode (WIP)
236 2019-09-26 07:41:18 +0200 LOLii: LED ports are starting with 100
235 2019-09-25 14:04:30 +0200 LOLii: type switch added
234 2019-09-25 10:20:15 +0200 LOLii: I2C set command added
233 2019-09-24 15:16:15 +0200 LOLii: two loco support (WIP)
232 2019-09-24 11:03:10 +0200 LOLii: RICi conf set added
231 2019-09-24 07:40:57 +0200 LOLii: fix for none pulse output
230 2019-09-23 15:02:31 +0200 LOLii: set ID added
229 2019-09-23 07:52:55 +0200 LOLii: set WiFi host name
228 2019-09-23 07:42:16 +0200 WIO: new schedule and pcb
227 2019-09-22 14:52:18 +0200 LOLii: steps in query
226 2019-09-22 14:06:40 +0200 LOLii: put loco addresses in the IO field in case of RICi
225 2019-09-21 15:11:11 +0200 LOLii: fix for case sensitive file system
224 2019-09-21 08:48:34 +0200 LOLii: test I/O layout reverted
223 2019-09-21 08:28:48 +0200 LOLii: fastled on D8
222 2019-09-19 08:06:42 +0200 WIO: LED range added
221 2019-09-18 14:55:45 +0200 WIO: query added
220 2019-09-18 11:17:40 +0200 color output support added
219 2019-09-17 15:36:05 +0200 wio: sensor ack added, and I2C scan from 100 to 10ms
218 2019-09-17 14:58:36 +0200 wio: sensor response (wip)
217 2019-09-17 14:21:51 +0200 wio: pulse length variable added
216 2019-09-17 14:11:50 +0200 wio: output pluse support
215 2019-09-17 11:34:03 +0200 wio: block timers
214 2019-09-17 10:36:13 +0200 wio: SoD fix
213 2019-09-17 08:36:50 +0200 wio: dout and accessory added
212 2019-09-16 15:28:17 +0200 LOLii: set LED mode to run in case of i2c
211 2019-09-16 14:42:03 +0200 LOLii: WIO support (wip)
210 2019-09-15 16:07:06 +0200 LOLii: WIO implementation (wip)
209 2019-09-15 08:27:48 +0200 LOLii: added som comment, and splitted up the loop into more functions
208 2019-09-15 07:49:17 +0200 LOLii: fix for light command
207 2019-09-14 15:38:29 +0200 update readme
206 2019-09-14 10:12:41 +0200 LOLii: rotary steps option added
205 2019-09-14 07:35:30 +0200 LOLii: version update
204 2019-09-14 07:29:54 +0200 LOLii: fix for flip light
203 2019-09-13 17:44:12 +0200 RICi: hardware
202 2019-09-13 10:14:26 +0200 LOLii: readme file added for initial default values
201 2019-09-12 10:47:51 +0200 LOLii: checkHeap function added for debugging
200 2019-09-12 10:38:04 +0200 LOLii: disable UDP Multicast to avoid draining heap space
199 2019-09-11 13:24:57 +0200 LOLii: wifi reconnect
198 2019-09-11 08:30:21 +0200 LOLii: lights by F3+F4
197 2019-09-10 14:28:48 +0200 LOLii: fix server reconnect
196 2019-09-10 11:19:43 +0200 LOLii: very fast flashing in case of no server connection
195 2019-09-10 08:57:59 +0200 LOLii: optional second loco
194 2019-09-10 08:09:37 +0200 LOLii: set LED to on at startup until wifi connection
193 2019-09-06 17:18:27 +0200 readme update
192 2019-09-06 15:57:56 +0200 LOLii: use discovered server name
191 2019-09-06 11:05:49 +0200 LOLii: ebreak added
190 2019-09-06 10:37:37 +0200 LOLii: r2rnet added
189 2019-09-05 14:19:40 +0200 LOLii: long click for function 5-8
