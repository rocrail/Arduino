/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_board_H_
#define _RCAN_board_H_

//---------- The base class for all boards.
class Board
{
private:

protected:

public:
  virtual bool Process(iCANFrame frame) = 0;
  virtual bool tick10ms(iCANFrame frame) = 0;
  virtual void SoD() = 0;
  virtual void init() = 0;
  virtual void publish() = 0;
  virtual void dumpPortState() = 0;
  virtual unsigned int getCANBPS() = 0;
  virtual void setID(byte id) = 0;
  virtual void setPort(byte port, byte conf) = 0;
  virtual byte getFilters(unsigned long* filters) = 0;
};


#endif
