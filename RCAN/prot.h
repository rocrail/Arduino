/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_prot_H_
#define _RCAN_prot_H_

//---------- The base interface for all protocols.
class Prot
{
private:

protected:

public:
  virtual bool evalFrame(iCANFrame frame, iCmdData cmd) = 0;
  virtual bool makeFrame(iCANFrame frame, iCmdData cmd) = 0;
  virtual int getCANBPS() = 0;
  virtual void setID(byte id) = 0;
  virtual byte getFilters(unsigned long* filters) = 0;
};

// Internal command and event codes
#define CMD_GETCV      1
#define CMD_SETCV      2
#define CMD_SOD        3
#define CMD_QUERY      4
#define CMD_PUBLISH    5
#define CMD_OUTPUT_ON  6
#define CMD_OUTPUT_OFF 7
#define EVT_SENSOR     8
#define EVT_OUTPUT     9
#define EVT_POWER_OFF  10
#define EVT_POWER_ON   11
#define CMD_QUERY_SENSOR   12
#define CMD_SIGNAL     13
#define ACK_SENSOR     14
#define CMD_ID         15
#define CMD_STATUS     16


#endif
