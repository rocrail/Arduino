/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _SERVOS_H_
#define _SERVOS_H_
#include <Servo.h>

#include "RCAN_dfs.h"
#include "cv.h"

#define MAX_SERVO 4
typedef struct _SERVOCNF {
  uint16_t gotopos;
  uint16_t curpos;
  uint16_t delay;
  uint16_t step;
  bool state;
  byte val;
  byte param;
} SERVOCNF;

class Servos
{
  byte m_ServoPins[MAX_SERVO];
  Servo* m_Servo[MAX_SERVO];
  SERVOCNF m_ServoCnf[MAX_SERVO];
public:
  Servos();
  bool tick10ms(iCANFrame frame, iCmdData cmd);
  bool doServo(iCANFrame frame, int port, byte pos, byte tick, byte val, byte param);

};

#endif
