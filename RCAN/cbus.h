/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_cbus_H_
#define _RCAN_cbus_H_

#include "RCAN_dfs.h"
#include "cv.h"
#include "prot.h"

// Config
#define CFG_FLAT 0x80 // flat model

class CBus : public Prot
{
private:
protected:
  byte m_CANID;
  byte m_BoardType;

public:
  CBus(byte ID, byte BoardType);
  bool evalFrame(iCANFrame frame, iCmdData cmd);
  bool makeFrame(iCANFrame frame, iCmdData cmd);
  int getCANBPS();
  void setID(byte id);
  byte getFilters(unsigned long* filters);
};


#endif
