/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2022 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _RCAN_gpio_H_
#define _RCAN_gpio_H_

#include "RCAN_dfs.h"
#include "board.h"
#include "prot.h"
#include "cv.h"
#include "io.h"
#include "servos.h"


// node config
#define CFG_SAVEOUTPUT     0x01
#define CFG_SHORTEVENTS    0x02
#define CFG_PULSETIME_MASK 0x0C
#define CFG_PULSETIME_250  0x00
#define CFG_PULSETIME_500  0x04
#define CFG_PULSETIME_1000 0x08
#define CFG_PULSETIME_2000 0x0C
#define CFG_SOD_REPORTALL  0x10
#define CFG_PULSETIME_LONG 0x20         // for 1500 3000 6000 12000ms Pulse Time


class GPIO : public Board
{
  Prot* m_Prot;
  unsigned char m_CANID;
  bool m_Ack;
  bool m_SoD;
  bool m_Publish;
private:
  unsigned long m_LastMillis;
  IO* m_IO;
  Servos* m_Servos;
  byte m_ScanIdx;
  bool setOutput(iCANFrame frame, iCmdData cmd, bool on);
  bool scanInputs(iCANFrame frame, iCmdData cmd);
  bool resetOutputs(iCANFrame frame, iCmdData cmd);
  void savePortState();
  void restorePortState();
  bool processSignal(iCANFrame frame, iCmdData cmd);
  bool doServo(iCANFrame frame, iCmdData cmd, byte val);
public:
  GPIO(byte prot, byte opt);
  bool Process(iCANFrame frame);
  bool tick10ms(iCANFrame frame);
  void SoD();
  void init();
  void publish();
  void dumpPortState();
  unsigned int getCANBPS();
  void setID(byte id);
  void setPort(byte port, byte conf);
  byte getFilters(unsigned long* filters);

};


#endif
