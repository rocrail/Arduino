#!/bin/sh
echo "const int revisionnr = " > gen/revision.h
git rev-list --count HEAD >> gen/revision.h
echo ";" >> gen/revision.h

git log -n 100 --abbrev-commit --oneline --format="%ci %s" | awk -v REV=`git rev-list --count HEAD` '{printf"%d %s\n",REV,$0;REV=REV-1;}' > gen/log.txt

